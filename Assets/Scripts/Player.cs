﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour {

    public float moveSpeed = 10f;
    public int startBlobs = 2;
    public float scalePerBlob = 1f;
    public float massPerBlob = 1f;
    public float angularDrag = 0.5f;
    private float curSpeed;
    private bool isSplited;
    private bool isPaused;
    private List<Transform> blobs = new List<Transform>();
    private Rigidbody2D myRb;
    private int curHp;
    private GameManager gameManager;

    private void Awake()
    {
        myRb = GetComponent<Rigidbody2D>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        myRb.angularDrag = angularDrag;
    }

    private void Start()
    {
        curSpeed = moveSpeed;
        curHp = GetConsumedBlobsCount();
    }

    void Update () {
        if (!isPaused)
        {
            SplitController();
        }
        if (curHp <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void FixedUpdate()
    {
        if (!isPaused)
        {
            PlayerController();
        }
    }

    void PlayerController()
    {
        float verical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");

        Vector2 move = new Vector2(horizontal, verical);

        myRb.velocity = Vector2.Lerp(myRb.velocity, move * curSpeed, 0.05f);

    }

    void SplitController()
    {
        if (Input.GetKeyDown("e"))
        {
            if (!isSplited)
            {
                Split();
            }
            else
            {
                Merge();
            }
        }
    }

    void Split()
    {
        isSplited = true;
        foreach(Transform blob in blobs)
        {
            if (!blob.gameObject.activeSelf)
            {
                blob.position = transform.position;
                blob.gameObject.SetActive(true);
            }
        }
        BlobsFollowPlayer();
        UpdateSize();
        myRb.AddForce(new Vector2(Random.Range(-50f, 50f), Random.Range(-50f, 50f)));
        curHp = GetConsumedBlobsCount();
    }

    void Merge()
    {
        isSplited = false;
        RushBlobsToPlayer();
        UpdateSize();
    }

    void RushBlobsToPlayer()
    {
        foreach (Transform blob in blobs)
        {
            if (blob.gameObject.activeSelf)
            {
                blob.GetComponent<PickupBlob>().RushToPlayer(true);
            }
        }
    }

    void BlobsFollowPlayer()
    {
        foreach (Transform blob in blobs)
        {
            if (blob.gameObject.activeSelf)
            {
                blob.GetComponent<PickupBlob>().RushToPlayer(false);
            }
        }
    }

    public void Hit()
    {
        curHp--;
    }

    public int PickUpBlob(Transform blob)
    {
        int blobId = 0;
        if (!blobs.Contains(blob))
        {
            blob.GetComponent<PickupBlob>().PickUp(transform);
            blobs.Add(blob);
        } else
        {
            blobId = blobs.IndexOf(blob);
        }

        if (!isSplited)
        {
            myRb.velocity = Vector2.zero;
            blob.gameObject.SetActive(false);
            curHp = GetConsumedBlobsCount();
        }
        UpdateSize();

        return blobId;

    }

    public void LoseBlob(Transform blob)
    {
        if (blobs.Contains(blob))
        {
            blobs.Remove(blob);
            Destroy(blob.gameObject);
        }
        UpdateSize();
    }

    void UpdateSize()
    {
        if (isSplited)
        {
            myRb.mass = massPerBlob;
            transform.localScale = new Vector3(scalePerBlob, scalePerBlob, scalePerBlob);
        } else
        {
            float blobsCount = GetConsumedBlobsCount();
            myRb.mass = blobsCount;
            transform.localScale = new Vector3(blobsCount, blobsCount, blobsCount);
        }
    }

    public int GetConsumedBlobsCount()
    {
        int count = 1;
        foreach(Transform blob in blobs)
        {
            if (!blob.gameObject.activeSelf)
            {
                count++;
            }
        }
        return count;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Key"))
        {
            gameManager.PickUpKey();
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.CompareTag("Finish"))
        {
            gameManager.NextLevel();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Breakable"))
        {
            if (GetConsumedBlobsCount() > 1)
            {
                Destroy(collision.gameObject);
            }
        }
    }
}
