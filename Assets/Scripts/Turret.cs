﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TurretState
{
    Idle,
    Spotted,
    Fire
}

public class Turret : MonoBehaviour {
    public Animator animator;
    public Transform projectileSpawn;
    public GameObject projectile;
    public float projectileSpeed = 35f;
    public float turnSpeed = 0.2f;
    private Transform target;
    public float spottedDistance = 10f;
    public float fireDistance = 5f;
    public TurretState turretState;

    private void Awake()
    {
        turretState = TurretState.Idle;
    }

    private void Update()
    {
        if (target == null)
        {
            turretState = TurretState.Idle;
            GameObject pl = GameObject.FindGameObjectWithTag("Player");
            if (pl != null)
            {
                target = pl.transform;
            }
            
        }
        StateController();
        Controll();
    }

    private void Controll()
    {
        switch (turretState)
        {
            case TurretState.Idle:
                Idle();
                break;
            case TurretState.Spotted:
                Spotted();
                break;
            case TurretState.Fire:
                Fire();
                break;
        }
    }

    private void Idle()
    {
        animator.SetBool("spotted", false);
        animator.SetBool("fire", false);
    }

    private void Spotted()
    {
        animator.SetBool("spotted", true);
        LookAtPlayer();
    }

    private void Fire()
    {
        animator.SetBool("fire", true);
        LookAtPlayer();
    }

    private void LookAtPlayer()
    {
        if (target != null)
        {
            Vector2 vTarget = target.position - transform.position;
            float angle = (Mathf.Atan2(vTarget.y, vTarget.x) * Mathf.Rad2Deg) -90;
            Quaternion qt = Quaternion.AngleAxis(angle, transform.forward);
            transform.rotation = Quaternion.Slerp(transform.rotation, qt, Time.deltaTime * turnSpeed);
        }
    }

    public void FireProjectile()
    {
        Projectile newProjectile = Instantiate(projectile, projectileSpawn.position, projectileSpawn.rotation).GetComponent<Projectile>();
        newProjectile.SetData(projectileSpeed, transform);
    }

    private void StateController()
    {
        if (target != null)
        {
            if (Vector2.Distance(transform.position, target.position) <= spottedDistance)
            {
                if (turretState == TurretState.Idle)
                {
                    Debug.Log("Spotted!");
                    turretState = TurretState.Spotted;
                }
                if (Vector2.Distance(transform.position, target.position) <= fireDistance)
                {
                    if (turretState == TurretState.Spotted)
                    {
                        Debug.Log("Fire!!");
                        turretState = TurretState.Fire;
                    }
                }
            }
            else
            {
                if (turretState != TurretState.Idle)
                {
                    turretState = TurretState.Idle;
                    Debug.Log("Idle!");
                }
            }

        }
    }

}
