﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    public GameObject keyIndicator;
    public GameObject exitOpen;
    public GameObject exitClosed;

    private bool hasKey;

    private void Awake()
    {
        //keyIndicator = GameObject.Find("GotKeyIndicator");
    }

    private void Start()
    {
        exitOpen.SetActive(false);
        exitClosed.SetActive(true);
    }

    public void PickUpKey()
    {
        hasKey = true;
        exitOpen.SetActive(true);
        exitClosed.SetActive(false);
        keyIndicator.SetActive(true);
    }

    private void Update()
    {
        if(Input.GetKeyDown("r"))
        {
            RestartLevel();
        }
    }

    public void NextLevel()
    {
        if (hasKey)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
