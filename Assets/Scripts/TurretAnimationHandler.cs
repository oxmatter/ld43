﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretAnimationHandler : MonoBehaviour {

    public Turret turret;

    public void FireProjectile()
    {
        turret.FireProjectile();
    }
}
