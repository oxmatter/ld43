﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {
    private float speed;
    private Rigidbody2D myRb;
    private Transform parent;

    private void Awake()
    {
        myRb = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        Destroy(gameObject, 5f);
    }

    private void FixedUpdate()
    {
        if (speed != 0 && myRb.velocity.magnitude <= speed)
        {
            myRb.velocity = transform.up * speed * Time.deltaTime;
        }
    }

    public void SetData(float speed, Transform parent)
    {
        this.speed = speed;
        this.parent = parent;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject collisionObj = collision.gameObject;
        if (collisionObj.CompareTag("Player"))
        {
            collisionObj.GetComponent<Player>().Hit();
        } else if (collisionObj.CompareTag("BlobPickup"))
        {
            collisionObj.GetComponent<PickupBlob>().Hit();
        }
        if (collision.transform != parent)
            Destroy(gameObject);
    }
}
