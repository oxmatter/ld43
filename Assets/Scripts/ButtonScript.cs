﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonScript : MonoBehaviour {

    public bool isPressed;
    public GameObject buttonOn;
    public GameObject buttonOff;
    public GameObject controlledObject;
    public float timeOut = 1f;
    private float timeCnt;

    private void Update()
    {
        if (isPressed)
        {
            timeCnt += Time.deltaTime;
            if (timeCnt >= timeOut)
            {
                ButtonPresed(false);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player") || collision.gameObject.CompareTag("BlobPickup"))
        {
            ButtonPresed(true);
        }
    }

    //private void OnCollisionExit2D(Collision2D collision)
    //{
    //    if (collision.gameObject.CompareTag("Player") || collision.gameObject.CompareTag("BlobPickup"))
    //    {
    //        ButtonPresed(false);
    //    }
    //}

    private void ButtonPresed(bool isPressed)
    {
        this.isPressed = isPressed;
        if (isPressed)
        {
            timeCnt = 0;
        }
        buttonOn.SetActive(isPressed);
        buttonOff.SetActive(!isPressed);
        controlledObject.SetActive(!isPressed);
    }
}
