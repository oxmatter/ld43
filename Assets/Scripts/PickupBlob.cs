﻿using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class PickupBlob : MonoBehaviour
{

    public float moveSpeed = 12f;
    public float rushSpeed = 20f;
    public float loseDistance = 5f;
    public float minDistanceToParent = 1.2f;
    public float pickUpDistance = 1.4f;
    public float angularDrag = 0.5f;

    private int curHp = 1;
    private bool isInRush;
    private bool pickedUp;
    private Transform parent;
    private Player player;
    private float curSpeed;
    private int blobId;

    private Collider2D myCol;
    private Rigidbody2D myRb;

    private void Awake()
    {
        myCol = GetComponent<Collider2D>();
        myRb = GetComponent<Rigidbody2D>();
        myRb.angularDrag = angularDrag;
    }

    public void PickUp(Transform parent)
    {
        this.parent = parent;
        pickedUp = true;
        myCol.isTrigger = false;
        myRb.isKinematic = false;
    }

    private void Update()
    {
        if (parent == null)
        {
            GameObject pl = GameObject.FindGameObjectWithTag("Player");
            if (pl != null)
            {
                parent = pl.transform;
            }
        }
        else
        {
            if (player == null)
            {
                player = parent.GetComponent<Player>();
            }
            CheckPickUp();
            CalculateSpeed();
            if (curHp <= 0)
            {
                Destroy(gameObject);
            }
        }
    }

    private void FixedUpdate()
    {
        if (parent != null)
            FollowParent();
    }

    public void RushToPlayer(bool rush)
    {
        isInRush = rush;
    }

    private void CheckPickUp()
    {
        if (Vector2.Distance(transform.position, parent.position) <= pickUpDistance * player.GetConsumedBlobsCount())
        {
            blobId = parent.GetComponent<Player>().PickUpBlob(transform);
        }
        if (pickedUp && Vector2.Distance(transform.position, parent.position) > loseDistance)
        {
            parent.GetComponent<Player>().LoseBlob(transform);
        }
    }

    private void FollowParent()
    {
        if (parent == null)
            return;
        if (pickedUp)
        {
            //Vector3 dir = (parent.transform.position - transform.position).normalized;
            //Vector3 dPos = curSpeed * dir * Time.deltaTime;
            Vector2 dir = parent.position - transform.position;
            Vector2 moveVel = dir * curSpeed * Time.deltaTime;
            //myRb.velocity = moveVel;
            myRb.velocity = Vector2.Lerp(myRb.velocity, moveVel, 0.05f);
            //myRb.MovePosition(transform.position + dPos);
        }
    }

    public void Hit()
    {
        curHp--;
    }

    private void CalculateSpeed()
    {
        if (parent == null)
            return;

        if (pickedUp)
        {
            if (isInRush)
            {
                curSpeed = rushSpeed;
            }
            else
            {
                if (Vector3.Distance(transform.position, parent.position) <= minDistanceToParent * (blobId +1))
                {
                    curSpeed = 0;
                }
                else
                {
                    curSpeed = moveSpeed;
                }
            }
        }
    }
}
