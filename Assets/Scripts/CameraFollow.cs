﻿using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public float followSpeed = 2f;
    public float zOffset = -10f;
    private Transform target;

    private void Update()
    {
        if (target != null)
        {
            Vector3 newPos = target.position;
            newPos.z = zOffset;
            transform.position = Vector3.Slerp(transform.position, newPos, followSpeed * Time.deltaTime);
        } else
        {
            GameObject playerObj = GameObject.FindGameObjectWithTag("Player");
            if (playerObj != null)
            {
                target = playerObj.transform;
            }
        }
    }
}
